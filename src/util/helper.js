export function round(val) {
	return (Math.round(val * 100) / 100).toFixed(2);
}
