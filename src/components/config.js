const config = {};

export const getConfig = (key) => config[key];

export const setConfig = (key, value) => (config[key] = value);
