import { getConfig } from '../../config';

var loadingPromise = null;

export const load = () => {
	if (!loadingPromise)
		loadingPromise = new Promise((resolve, reject) => {
			const zingchartScript = document.createElement('SCRIPT');

			let url = '/js/zingchart.min.js';

			zingchartScript.setAttribute('src', url);
			zingchartScript.setAttribute('async', '');
			zingchartScript.setAttribute('defer', '');

			document.head.appendChild(zingchartScript);

			zingchartScript.addEventListener('load', () => {
				zingchart.LICENSE = getConfig('license');
				let Export = getConfig('export'),
					AjaxExport = getConfig('ajaxExport');

				if (Export) zingchart.EXPORTURL = Export;

				if (AjaxExport) zingchart.AJAXEXPORT = AjaxExport;

				resolve();
			});

			zingchartScript.addEventListener('error', (err) => {
				reject(`couldn't find zingchart.min.js in /public/js/zingchart.min.js`);
			});
		});
	return loadingPromise;
};
