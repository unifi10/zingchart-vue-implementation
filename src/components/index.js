import baseZingChart from './BaseZingchart/index';
import gaugeChart from './GaugeChart/index';
import lineAreaBarChart from './LineAreaBarChart/index';
import donutChart from './DonutChart/index';
import { setConfig } from './config';

export const zingchart = baseZingChart;
export const GaugeChart = gaugeChart;
export const LineAreaBarChart = lineAreaBarChart;
export const DonutChart = donutChart;

export default {
	zingchart,
	install(options = {}) {
		setConfig('license', options.license);
		setConfig('export', options.export);
		setConfig('ajaxExport', options.ajaxExport);
	},
};
